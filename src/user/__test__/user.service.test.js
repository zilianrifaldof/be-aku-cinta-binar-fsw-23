const { createUser, getAllUser, editUser, findEmail, editPassword, getUserById, userAddScore } = require("../user.service");
const { faker } = require("@faker-js/faker");
const bcrypt = require("bcrypt");
const userRepo = require("../user.repo");
const nodemailer = require('nodemailer');




const testData = {
    fullname : faker.name.fullName(),
    email : faker.internet.email(),
    password: faker.internet.password()
};

bcrypt.hash = jest.fn(() => "test");

describe('user.service.js', () => {
    describe('#getAllUser', () => {
        it('should return a list of all user', async () => {

            const result = await getAllUser({pageNumber: 1, limitParm: 5});
            expect(typeof result).toBe('object');
            expect(result).not.toBeNull();
        });
        it('should show error if no args were given', async () => {
            try{
                const reult = await getAllUser();
            }catch(e){
                expect(e.name).toBe('TypeError');
            }
        });
    });
    describe('#createUser', ()=> {
        it('should create a new user', async () => {
            userRepo.checkEmailAllUser = jest.fn(()=> null);

            const result = await createUser(testData);
            expect(bcrypt.hash).toBeCalledWith(testData.password, 10);
            expect(result.fullname).toBe(testData.fullname);
            expect(result.email).toBe(testData.email);
        });
        it('should return null because created with registered email', async () => {
            userRepo.checkEmailAllUser = jest.fn(()=> {
                return {fullname: 'registered'}
            });

            const result = await createUser(testData);
            expect(bcrypt.hash).toBeCalledWith(testData.password, 10);
            expect(userRepo.checkEmailAllUser).toBeCalledWith(testData.email);
            expect(result).toBeNull();
        });
    });
    describe('#editUser', () => {
        const testEditUser = {
            fullname: faker.name.fullName(),
            email: faker.internet.email(),
            userId : 3,
            authUserId : 3
        }
        it('should edit the user', async () => {
            userRepo.checkEmailAllUser = jest.fn(()=> null);
            userRepo.checkSameEmail = jest.fn(()=>null);

            const result = await editUser({fullname : testEditUser.fullname, email: testEditUser.email, userId: testEditUser.userId, authUserId: testEditUser.authUserId}); 
            expect(userRepo.checkEmailAllUser).toBeCalledWith(testEditUser.email);
            expect(userRepo.checkSameEmail).toBeCalledWith({email: testEditUser.email, authUserId: testEditUser.authUserId});
            expect(result).toBe("Update successful");
        })
        it('should not edit the user', async () => {
            userRepo.checkEmailAllUser = jest.fn(()=> {
                return {
                    fullname: 'registered'
                }
            });
            userRepo.checkSameEmail = jest.fn(()=>null);

            const result = await editUser({ fullname : testEditUser.fullname, email: testData.email, userId: testEditUser.userId, authUserId: testEditUser.authUserId});
            expect(userRepo.checkEmailAllUser).toBeCalledWith(testData.email);
            expect(userRepo.checkSameEmail).toBeCalledWith({email: testData.email, authUserId: testEditUser.authUserId});
            expect(result).toBe('This email is already being used, Please choose another email')
        });
    });
    describe('#findEmail', () => {
        userRepo.checkEmailAllUser = jest.fn();
        const emailTest = {
            id:2,
            fullname: 'registered',
            email: 'fake1@mail.com'
        };
        it('should send the user password reset email', async () => {
            jest.mock('nodemailer', () => ({
                createTransport: jest.fn().mockReturnValue({
                    sendMail: jest.fn().mockReturnValue((mailoptions, callback) => {})
                  })
            }));
            userRepo.checkEmailAllUser.mockReturnValue(emailTest);
            userRepo.editPassword = jest.fn(()=>[1]);

            const result = await findEmail ({email: testData.email});
            expect(userRepo.checkEmailAllUser).toBeCalled();
            expect(result).toBe(emailTest.email);
        });
        it('should not send the user the reset email', async () => {
            userRepo.checkEmailAllUser.mockReturnValue(null);

            const result = await findEmail ({email: testData.email});
            expect(userRepo.checkEmailAllUser).toBeCalled();
            expect(result).toBe(null);
        });
    });
    describe('#editPassword', () => {
        it('should edit the user by userId and new Password', async () => {
            const testEditUser = {
                userId : 3,
                password : '112233'
            }

            const result = await editPassword(testEditUser);
            expect(bcrypt.hash).toBeCalledWith(testEditUser.password,10);
            expect(result[0]).toStrictEqual(1);
        });
    });
    describe('#getUserById', () => {
        it('should get the user by the ID', async () => {
            const testEditUser = {
                userId : 1
            }

            const result = await getUserById(testEditUser);
            expect(result.fullname).toBe('FirstUser');
            expect(result.email).toBe('example@example.com');
            expect(result).not.toBeNull();
        });
    });
    describe('#userAddScore', () => {
        it('should update the user score', async () => {
            userRepo.getUserById = jest.fn(()=> {
                return{ 
                    score:2
                }
            });

            const result = await userAddScore(1);
            expect(userRepo.getUserById).toBeCalledWith({userId: 1})
            expect(result[0]).toBe(1);
        })
        
    })
})